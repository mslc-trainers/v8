package com.verizon.learning.model;

import java.util.List;

public class Customer {

	private String id;
	private String name;
	private List<CustomerAddress> addresses;

	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public Customer(String id, String name) {

		this.id = id;
		this.name = name;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CustomerAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<CustomerAddress> addresses) {
		this.addresses = addresses;
	}
}
