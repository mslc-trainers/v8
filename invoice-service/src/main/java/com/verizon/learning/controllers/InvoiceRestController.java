package com.verizon.learning.controllers;

import com.verizon.learning.model.Customer;
import com.verizon.learning.model.CustomerAddress;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Arrays;
import java.util.List;

@RestController
public class InvoiceRestController {

    WebClient webClient = WebClient.create("http://localhost:8080");


    private Flux<CustomerAddress> getDeliveryAddresses(String customerId) {

        return
                webClient
                        .get()
                        .uri(uriBuilder -> {

                            return
                                    uriBuilder
                                            .path("/customers/{customerId}/addresses")
                                            .queryParam("type", "D")
                                            .build(customerId);
                        })
                        .retrieve()
                        .bodyToFlux(CustomerAddress.class);

    }

    private Flux<CustomerAddress> getPostalAddresses(String customerId) {


        return
                webClient
                        .get()
                        .uri(uriBuilder -> {

                            return
                                    uriBuilder
                                            .path("/customers/{customerId}/addresses")
                                            .queryParam("type", "P")
                                            .build(customerId);
                        })
                        .retrieve()
                        .bodyToFlux(CustomerAddress.class);

    }


    private Mono<Customer> getCustomerById(String customerId) {

        Mono<Customer> monoCustomer = webClient
                .get()
                .uri("/customers/{customerId}", customerId)
                .retrieve()
                .bodyToMono(Customer.class);

        return monoCustomer;
    }

    @GetMapping(path = "/invoice-customers")
    public Flux<Customer> handleGetInvoiceCustomers() {

        // http://localhost:8080/customers

        // @formatter:off


//		RestTemplate template = new RestTemplate();
//		ResponseEntity<List<Customer>> customerResponse = 
//				
//				template.exchange("http://localhost:8080/customers",
//									HttpMethod.GET,
//									null, new ParameterizedTypeReference<List<Customer>>() {
//									});
//		
//		List<Customer> customers = customerResponse.getBody();
//		System.out.println(customerResponse.getBody());

        Flux<Customer> customers = webClient
                .get()
                .uri("/customers")
                .retrieve()
                .bodyToFlux(Customer.class);


        return customers;
    }


    /**
     *
     */
    @GetMapping(path = "/customers/{customerId}")
    public Mono<Customer> handleGetCustomerById(@PathVariable String customerId) {

        // @formatter:off

        Mono<Customer> customer =
                webClient
                        .get()
                        .uri("/customers/{customerId}", customerId)
                        .retrieve()
                        .bodyToMono(Customer.class);

        return customer;

    }

    @GetMapping(path = "/select-customers")
    public Flux<Customer> handleGetSelectCustomers() {

        List<String> customerIds = Arrays.asList("101", "103", "105");

//        Flux<String> fStrings =Flux.fromIterable(customerIds);

//        Flux<Mono<Customer>> customers = Flux.fromIterable(customerIds)
//                .map(x -> {
//
//                    Mono<Customer> monoCustomer = webClient
//                            .get()
//                            .uri("/customers/{customerId}", x)
//                            .retrieve()
//                            .bodyToMono(Customer.class);
//
//                    return monoCustomer;
//
//                });

        Flux<Customer> customers =

                Flux.fromIterable(customerIds)
                        .flatMap(this::getCustomerById);


//        Flux<Customer> customers = null;

        return customers;
    }


    @GetMapping(path = "/addresses")
    public Flux<CustomerAddress> handleGetAddresses() {

        List<String> customerIds = Arrays.asList("101", "103", "104");
        /**
         * http://localhost:8080/customers/{103}/addresses?type=D
         */

//        Flux<Flux<CustomerAddress>> allAddresses = Flux.fromIterable(customerIds)
//                .map(x -> {
//
//                    Flux<CustomerAddress> deliveryAddresses = this.getDeliveryAddresses(x);
//                    Flux<CustomerAddress> postalAddresses = this.getPostalAddresses(x);
//
//                    Flux<CustomerAddress> customerAddresses = Flux.concat(deliveryAddresses, postalAddresses);
//
//                    return customerAddresses;
//
//                });

//
//        Flux<CustomerAddress> allAddresses = Flux.fromIterable(customerIds)
//                .flatMap(x -> {
//
//                    Flux<CustomerAddress> deliveryAddresses = this.getDeliveryAddresses(x);
//                    Flux<CustomerAddress> postalAddresses = this.getPostalAddresses(x);
//
//                    Flux<CustomerAddress> customerAddresses = Flux.concat(deliveryAddresses, postalAddresses);
//
//                    return customerAddresses;
//
//                });


        Flux<CustomerAddress> pAddresses = Flux.fromIterable(customerIds)
                .flatMap(this::getPostalAddresses);

        Flux<CustomerAddress> dAddresses = Flux.fromIterable(customerIds)
                .flatMap(this::getDeliveryAddresses);

        return pAddresses.concat(dAddresses);
    }


    @GetMapping(path = "/address-map")
    public Flux<Customer> handleGetCustomerAddressMap() {

        List<String> customerIds = Arrays.asList("101", "102", "103", "104", "105");
        /**
         *
         * /customers/{customerId}
         * /customers/{customerId}/addresses?type=D
         *
         */


//        Mono<Tuple2<Customer, List<CustomerAddress>>> customer =
//                this.getCustomerById("101")
//                        .zipWhen(x -> {
//
//                            String customerId = x.getId();
//                            Flux<CustomerAddress> addresses = this.getDeliveryAddresses(customerId);
//                            Mono<List<CustomerAddress>> addressList = addresses.collectList();
//                            return addressList;
//
//                        });


        Flux<String> f1 = null;


        Mono<String> f2 = null;


        Flux<Customer> customers =

                Flux.fromIterable(customerIds)
                        .flatMap(id ->
                                {
                                    Mono<Customer> customer =
                                            this.getCustomerById(id)
                                                    .zipWhen(x -> {

                                                        String customerId = x.getId();
                                                        Flux<CustomerAddress> dAddresses = this.getDeliveryAddresses(customerId);
                                                        Flux<CustomerAddress> pAddresses = this.getPostalAddresses(customerId);


                                                        Mono<List<CustomerAddress>> addressList =
                                                                dAddresses
                                                                        .concat(pAddresses).collectList();

                                                        return addressList;

                                                    })
                                                    .map(x -> {

                                                        Customer c = x.getT1();
                                                        List<CustomerAddress> a = x.getT2();

                                                        c.setAddresses(a);
                                                        return c;
                                                    });
                                    return customer;
                                }
                        );


        return customers;


    }


}
