package com.verizon.learning.controllers;

import com.verizon.learning.model.Customer;
import com.verizon.learning.model.CustomerAddress;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Demo {

    public static void main(String[] args) {

        List<Customer> customers = new ArrayList<>();

        /**
         * Transformed from Customer >> Customer Names
         */
        List<String> customerNames =
                customers
                        .stream()
                        .map(x -> x.getName())
                        .collect(Collectors.toList());

        /**
         * Transformed from Customer >> Customer Ids in Integer
         */
        List<Integer> customerIds =
                customers
                        .stream()
                        .map(x -> x.getId())
                        .map(x -> Integer.valueOf(x))
                        .collect(Collectors.toList());

        /**
         * Transformed from Customer >> CustomerAddresses
         */
//        List<List<CustomerAddress>> customerAddresses =
//                customers
//                        .stream()
//                        .map(x -> x.getAddresses())
//                        .collect(Collectors.toList());


        List<CustomerAddress> customerAddresses =
                customers
                        .stream()
                        .flatMap(x -> x.getAddresses().stream())
                        .collect(Collectors.toList());




    }
}
