package com.verizon.learning;

import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;

public class BackPressureDemo {

    public static void main(String[] args) {


        Flux<Integer> fInts = Flux.range(0, 15);

        fInts
                .log()
                .subscribe(new BaseSubscriber<Integer>() {

                    @Override
                    protected void hookOnSubscribe(Subscription subscription) {
                        subscription.request(6);
//                        super.hookOnSubscribe(subscription);
                    }

                    @Override
                    protected void hookOnNext(Integer value) {
//                        System.out.println(value);
                        if (value > 0 && value % 5 == 0) {

                            System.out.println("Will request for another 5 after 5 seconds");
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            super.request(5);
                        }
                    }
                });


    }
}
