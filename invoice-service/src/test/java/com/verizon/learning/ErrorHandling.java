package com.verizon.learning;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import java.sql.SQLOutput;

public class ErrorHandling {


    @Test
    public void on_error_continue() {

        Flux<Integer> fInts = Flux
                .range(0, 10)
                .map(x -> {
                    if (x == 3) {
                        throw new RuntimeException("Some error encountered...");
                    }
                    return x + 1;
                })
                .onErrorContinue((x, y) -> {

                    System.out.println(x.getMessage() + " -- " + y);

                })
                .log();

        fInts.subscribe();

    }

    @Test
    public void on_error_resume() {

        Flux<Integer> fInts = Flux
                .range(0, 10)
                .map(x -> {
                    if (x == 3) {
                        throw new RuntimeException("Some error encountered...");
                    }
                    return x + 1;
                })
                .onErrorResume(x -> {
                    System.out.println(x.getMessage());
                    return Flux.just(200);
                })
                .log();

        fInts.subscribe();

    }
    @Test
    public void on_error_return() {

        Flux<Integer> fInts = Flux
                .range(0, 10)
                .map(x -> {
                    if (x == 3) {
                        throw new RuntimeException("Some error encountered...");
                    }
                    return x + 1;
                })
                .onErrorReturn(200)
                .log();

        fInts.subscribe();

    }

    public static void main(String[] args) {


        Flux<Integer> fInts = Flux
                .range(0, 10)
                .map(x -> {
                    if (x == 3) {
                        throw new RuntimeException("Some error encountered...");
                    }
                    return x + 1;
                });

        fInts.subscribe(x -> {
                    System.out.println(x);
                },
                x -> {
                    System.out.println("Error : " + x.getMessage());
                },
                () -> {
                    System.out.println("Completed....");
                });
    }
}
