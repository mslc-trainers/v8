package com.verizon.learning;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

import java.time.Duration;

public class ZipDemo {

    @Test
    public void zip_demo() throws InterruptedException {

        Flux<Long> f1 = Flux
                .interval(Duration.ofSeconds(1));

        Flux<String> f2 = Flux.just("f2-0", "f2-1", "f2-2");

//        Flux<Tuple2<Long, String>> f3 = f1.zipWith(f2);

        Flux<String> f3 = f1.zipWith(f2, (x , y) -> {

            return x + "**" + y;
        });

        f3.log().subscribe();

        Thread.sleep(10000);
    }
}

