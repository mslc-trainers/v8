package com.verizon.learning;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class ConcatAndMergeDemo {


    @Test
    public void runon_subscribeon_demo() throws InterruptedException {


//        Flux<Integer> f1 = Flux
//                .just(101, 102, 103)
//                .parallel()
//                .runOn(Schedulers.parallel())
//                .map(x -> {
//                    int val = x + 1;
//                    System.out.println(val + " map in : " + Thread.currentThread().getName());
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    return val;
//                })
//                .sequential()
//                .publishOn(Schedulers.elastic());



        Flux<Integer> f1 = Flux
                .just(101, 102, 103)
                .map(x -> {
                    int val = x + 1;
                    System.out.println(val + " map in : " + Thread.currentThread().getName());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return val;
                })
                .subscribeOn(Schedulers.parallel());




        f1.subscribe(x -> {
            System.out.println(x + " received in " + Thread.currentThread().getName());
        });

        Thread.sleep(10000);


    }


    @Test
    public void when_concat() {

        Flux<String> f1 = Flux.just("101", "102", "103");

        Flux<String> f2 = Flux
                .just("201", "202", "203")
                .map(x -> {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return (Integer.valueOf(x) + 5) + "";
                });

        Flux<String> concatenatedFlux = Flux.concat(f2, f1);


        concatenatedFlux
                .subscribe(x -> {
                    System.out.println(x);
                });


    }

    @Test
    public void when_merge() throws InterruptedException {

        Flux<String> f1 = Flux.just("101", "102", "103")
                .map(x -> {

                    try {

                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName());

                    return (Integer.valueOf(x) + 10) + "";
                })
                .subscribeOn(Schedulers.parallel());

        System.out.println("After creating f1....");

        Flux<String> f2 = Flux
                .just("201", "202", "203")
                .map(x -> {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return (Integer.valueOf(x) + 5) + "";
                })
                .subscribeOn(Schedulers.parallel());

        Flux<String> interleavedMergeSequence = Flux.merge(f2, f1);


        interleavedMergeSequence
                .subscribe(x -> {
                    System.out.println(x);
                });

        Thread.sleep(10000);

    }


}
