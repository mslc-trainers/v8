package com.verizon.learning;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class FluxCreationTest {
	
	
	/**
	 * 	Flux<String> f1 = Flux.fromArray(new String[] {"s1", "s2", "s3"});
		
//		Flux<String> f2 = Flux.fromIterable(Arrays.as)
		
//		Flux<Integer> f3 = Flux.range(start, count)
		
//		Flux<Long> f4 = Flux.interval(Duration.of)
		
//		Flux<String> f5 = Flux.fromStream(s)
	 */
	
	@Test
	public void when_from_array() {
		Flux<String> f1 = Flux.fromArray(new String[] {"s1", "s2", "s3"});
		
		// @formatter:off

		StepVerifier
		      .create(f1)
		      .expectSubscription()
		      .expectNextCount(3)
		      .expectComplete()
		      .verify();
		 
		// @formatter:on
		
		
		
	}
	
	

}
