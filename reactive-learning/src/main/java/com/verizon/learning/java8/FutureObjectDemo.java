package com.verizon.learning.java8;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureObjectDemo {

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService threadPoolService = Executors.newFixedThreadPool(5);

//		threadPoolService.execute(new Runnable() {
//
//			@Override
//			public void run() {
//				//
//				System.out.println("Task executing in Thread : " + Thread.currentThread().getName());
//				
//			}
//			
//			
//		});
//

		Future<String> futureObject = threadPoolService.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				// service 1
				System.out.println("the task has started in : " + Thread.currentThread().getName());
				Thread.sleep(5000);
				
				

				return "response1";
			}

		});

		
		/**
		 * Blocking Call
		 */
		String value = futureObject.get();
		System.out.println(value);
		
		
		/**
		 * 
		 */
		
		
		
		
		threadPoolService.shutdown();

	}

}
