package com.verizon.learning.java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsDemo {

	public static void main(String[] args) {

		List<String> clients = Arrays.asList("nomura", "morgan stanley", "verizon", "jp morgan", "ibm", "wipro");

		// @formatter:off

		long start = System.currentTimeMillis();
	    List<String> upperCasedClients = 
			
			clients
			   .stream()
			   .parallel()
			   .map(x -> {
				   
				   System.out.println("Transformation / map is happening in : " + Thread.currentThread().getName());
				   try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				   return x.toUpperCase();
			   })
			   .collect(Collectors.toList());

		
		
		// @formatter:on
	    long end = System.currentTimeMillis();
		System.out.println("Time Taken is : " + (end - start) +  " -- " +    upperCasedClients);

	}

}
