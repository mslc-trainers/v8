package com.verizon.learning.java8;

import java.util.ArrayList;
import java.util.List;

public class ExternalServiceCallsDemo {
	
	
	public static void main(String[] args) {
		
		// service 1 : https://google.com/distance-api
		   // : response1 
		
		// service 2 : https://api.verizon.com/mapping-solution
		  // : response2
		
		List<Object> values = new ArrayList<>();
		
		new Thread() {
			@Override
			public void run() {
				
				// service 1
				// response1
				values.add("response1");
			}
		}.start();
		
		new Thread() {
			@Override
			public void run() {
				
				// service 2
				// response 2
				values.add("response2");
			}
		}.start();
	
		
		
		
	}

}
