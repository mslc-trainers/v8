package com.verizon.learning.java8;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceDemo {

	public static void main(String[] args) throws InterruptedException {

		ExecutorService service = Executors.newCachedThreadPool();
		
		System.out.println("Cores on my machine : " + Runtime.getRuntime().availableProcessors());
		
		int numberOfTasks = 64;
		CountDownLatch latch = new CountDownLatch(numberOfTasks);
		
		long start = System.currentTimeMillis();
		for (int i = 0; i < numberOfTasks; i++) {
			service.execute(new Runnable() {
				@Override
				public void run() {

					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("The Thread : " + Thread.currentThread().getName() + " is executing the task");

					
					latch.countDown();
				}
			});
		}
		
		latch.await();
		long end = System.currentTimeMillis();
		
		System.out.println("Total time taken is : " + (end - start));
		service.shutdown();

	}

}
