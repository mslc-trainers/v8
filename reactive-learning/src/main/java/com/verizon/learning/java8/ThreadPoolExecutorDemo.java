package com.verizon.learning.java8;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutorDemo {

	public static void main(String[] args) {

		ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 10, 3, TimeUnit.SECONDS,
				    new ArrayBlockingQueue<>(100), new ThreadFactory() {
						
						@Override
						public Thread newThread(Runnable r) {
							Thread t1 = new Thread(r) ;
							
							return t1;
						}
					}, new RejectedExecutionHandler() {
						
						@Override
						public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
							//
							
						}
					});
		
		
		executor.execute(new Runnable() {

			@Override
			public void run() {
				
				//
				
			}
			
		});
		
		

	}

}
