package com.verizon.learning.java8;

public class DeamonThreadDemo {

	public static void main(String[] args) {

		new Thread() {

			
			{
				super.setDaemon(true);
			}
			
			
			@Override
			public void run() {
				
				System.out.println("Thread : " + Thread.currentThread().getName() + " Started " + Thread.currentThread().isDaemon());

				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.out.println("Thread : " + Thread.currentThread().getName() + " is executed...");
			}
		}.start();

		System.out.println("Thread : " + Thread.currentThread().getName());

	}

}
