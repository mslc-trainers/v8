package com.verizon.learning.java8;

public class BlockedStateDemo {

	public static void main(String[] args) throws InterruptedException {

		Counter c = new Counter();

		Thread t1 = new Thread() {

			public void run() {
				c.increment();
			};
		};

		t1.start();
		Thread.sleep(100);
		
		Thread t2 = new Thread() {

			public void run() {
				c.increment();
			};
		};
		t2.start();
		
		
		new Thread() {
			public void run() {
				while (true) {

					System.out.println("t2 state : " + t2.getState().name());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			};
		}.start();

	}

}

class Counter {

	private int i;

	public synchronized void increment() {
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		i++;
	}

}
