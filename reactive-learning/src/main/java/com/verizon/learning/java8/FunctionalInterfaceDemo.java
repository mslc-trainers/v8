package com.verizon.learning.java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class FunctionalInterfaceDemo {

	public static void main(String[] args) {

//		TaxCalculator t1 =  () -> Float.valueOf("23");

		generateTaxSlips(() -> {

			return 10;
		});

		List<String> clients = Arrays.asList("nomura", "morgan stanley", "verizon", "jp morgan", "ibm", "wipro");
		
		Stream<String> clientStream = clients.stream();
	
	}

	/**
	 * Higher Order Function
	 * 
	 * @param calculator
	 */
	public static void generateTaxSlips(TaxCalculator calculator) {

	}

}

interface TaxCalculator {

//	public float calculateTax(String state, int baseRate);
//	public void calculateTax(String state, int baseRate);
	public float calculateTax();

}
