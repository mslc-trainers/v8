package com.verizon.learning.java8;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

public class CompletableFutureDemo {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		System.out.println(Runtime.getRuntime().availableProcessors());

		System.out.println(ForkJoinPool.commonPool().getParallelism());

		CompletableFuture<String> cf = CompletableFuture.supplyAsync(new Supplier<String>() {

			@Override
			public String get() {
				// service 1
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(Thread.currentThread().getName() + " executes the task");
				return "response1";

			}
		});

		/**
		 * 
		 * Non-Blocking.....
		 * 
		 */
		cf.whenComplete(new BiConsumer<String, Throwable>() {

			@Override
			public void accept(String t, Throwable u) {
				
				// service 2
				
				if (u == null) {
					System.out.println("the value received is : " + t);	
				} else {
					System.out.println(u.getMessage());
				}
				

			}
		});

		System.out.println("I am in main : " + Thread.currentThread().getName());

		Thread.sleep(10000);

	}

}
