package com.verizon.learning.java8;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * NEW !
 * 
 * RUNNABLE !
 * 
 * WAITING !
 * 
 * TIMED_WAITING !
 * 
 * BLOCKED !
 * 
 * TERMINATED !
 * 
 * 
 * 
 * @author shakir
 *
 */
public class ThreadStateDemo {

	public static void main(String[] args) throws Throwable {

		ServerSocket ss = new ServerSocket(7001);

		Thread t1 = new Thread() {

			public void run() {

				try {
					System.out.println("Accepting requests..");
					Socket s = ss.accept();
					System.out.println("Request received.....");

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			};
		};

		t1.start();

		new Thread() {
			public void run() {
				while (true) {

					System.out.println("t1 state : " + t1.getState().name());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			};
		}.start();

	}

}
