package com.verizon.learning.reactor;

import java.util.concurrent.CountDownLatch;

import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

public class SchedulersDemo {

	public static void main(String[] args) throws InterruptedException {

//		ExecutorService service = Executors.newCachedThreadPool();

//		Scheduler scheduler = Schedulers.parallel();
//		Scheduler scheduler = Schedulers.newParallel(32, new ThreadFactory() {
//			
//			@Override
//			public Thread newThread(Runnable r) {
//				
//				return new Thread(r);
//			}
//		});
		
		
		Scheduler scheduler = Schedulers.boundedElastic();
//		Scheduler scheduler = Schedulers.newBoundedElastic(200, 500, "yabadabadoo");
		
//		Scheduler scheduler = Schedulers.newBoundedElastic(threadCap, queuedTaskCap, name, ttlSeconds, daemon)
		
		System.out.println("Cores on my machine : " + Runtime.getRuntime().availableProcessors());

		int numberOfTasks = 320;
		CountDownLatch latch = new CountDownLatch(numberOfTasks);

		long start = System.currentTimeMillis();
		for (int i = 0; i < numberOfTasks; i++) {
			scheduler.schedule(new Runnable() {
				@Override
				public void run() {

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().isDaemon() +    " The Thread : " + Thread.currentThread().getName() + " is executing the task");

					latch.countDown();
				}
			});
		}

		latch.await();
		long end = System.currentTimeMillis();

		System.out.println("Total time taken is : " + (end - start));
		

	}

}
