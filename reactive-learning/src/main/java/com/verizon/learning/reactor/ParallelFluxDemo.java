package com.verizon.learning.reactor;

import java.util.Arrays;
import java.util.List;

import reactor.core.publisher.Flux;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Schedulers;

public class ParallelFluxDemo {

	public static void main(String[] args) throws InterruptedException {

		List<String> clients = Arrays.asList("nomura", "morgan stanley", "verizon", "jp morgan", "ibm", "wipro");

		Flux<String> clientsFlux = Flux.fromIterable(clients);

		ParallelFlux<String> upperCasedClients =

				clientsFlux
				  .parallel()
				  .runOn(Schedulers.newBoundedElastic(50, 100, "yaba"))
				  .map(x -> {

					System.out.println("Transformation / map is happening in : " + Thread.currentThread().getName());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return x.toUpperCase();
				});

		upperCasedClients.subscribe(x -> {

			System.out.println(x);
		});

		Thread.sleep(10000);
	}

}
