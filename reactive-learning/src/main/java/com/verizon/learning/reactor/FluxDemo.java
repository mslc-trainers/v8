package com.verizon.learning.reactor;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import reactor.core.publisher.Flux;

public class FluxDemo {

	public static void main(String[] args) {

		Flux<String> f1 = Flux.just("nomura", "morgan stanley", "verizon", "jp morgan", "ibm", "wipro");

		// @formatter:off

		f1
		  .map(x -> x.toUpperCase())
//		  .filter(t -> t.contains("MORGAN"))
		  .subscribe(value -> {System.out.println(value); },
				     error  -> { System.out.println(error.getMessage());   },
				     () -> {System.out.println("completed....");});
		
		 
		// @formatter:on

		Function<String, Integer> f2 = x -> x.length();
		BiFunction<String, String, Integer> f3 = (x, y) -> x.length() + y.length();
		Consumer<String> f4 = x -> {
			System.out.println(x);
		};
		Supplier<String> f5 = () -> new String("verizon");
		BiConsumer<String, String> f6 = (x, y) -> {
		};
		BiPredicate<String, String> f7 = (x, y) -> x.length() > y.length();

	}

}
