package com.verizon.learning.controllers;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.verizon.learning.model.Customer;
import com.verizon.learning.model.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class CustomerRestController {

	@Autowired
	CustomerRepository customerRepo;

	@GetMapping(path = "/customers", produces = { MediaType.APPLICATION_STREAM_JSON_VALUE })
	public Flux<Customer> handleGetCustomers() {

//		System.out.println("Thread that recieved the request : " + Thread.currentThread().getName());
////		List<String> customers = Arrays.asList("nomura", "morgan stanley", "verizon", "jp morgan", "ibm", "wipro");
//		
//		Flux<Long> customerResponse = 
//				      Flux
//				        .interval(Duration.ofSeconds(1))
//				        .take(10)
//				        .log();
//		
//		
//		System.out.println("Thread that recieved the request is moving out : " + Thread.currentThread().getName());

		Flux<Customer> customerResponse = customerRepo.findAll();

		return customerResponse.delayElements(Duration.ofMillis(1));
	}

	@GetMapping(path = "/customers/{customerId}")
	public Mono<ResponseEntity<Customer>> handleGetCustomerById(@PathVariable String customerId) {

		Mono<Customer> monoCustomer = customerRepo.findById(customerId);

//		Customer customer = monoCustomer.block();
//
//		Mono<ResponseEntity<Customer>> response = null;
//		if (customer != null) {
//			response = Mono.just(new ResponseEntity<>(customer, HttpStatus.OK));
//		} else {
//			response = Mono.just(new ResponseEntity<>(HttpStatus.NO_CONTENT));
//		}
		
		// @formatter:off

		Mono<ResponseEntity<Customer>> response = 				
				monoCustomer
				    .map(x -> new ResponseEntity<>(x, HttpStatus.OK))
				    .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
		

		return response;
	}

}
