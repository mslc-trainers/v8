package com.verizon.learning.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.verizon.learning.model.CustomerAddress;
import com.verizon.learning.model.CustomerAddressRepository;

import reactor.core.publisher.Flux;

@RestController
public class CustomerAddressRestController {

	@Autowired
	CustomerAddressRepository addressRepo;

	@GetMapping(path = "/addresses")
	public Flux<CustomerAddress> handleGetAddresses() {

		return addressRepo.findAll();
	}

	/**
	 * localhost:8080/customers/102/addresses?type=P
	 * 
	 */

	@GetMapping(path = "/customers/{customerId}/addresses", params = { "type" })
	public Flux<CustomerAddress> handleGetCustomerAddresses(@PathVariable String customerId,
			@RequestParam String type) {

		// @formatter:off

//		Flux<CustomerAddress> addresses = 
//				  addressRepo
//				     .findAll()
//				     .filter(x -> x.getCustomerId().equals(customerId)  && x.getType().equals(type) );
//				     
		Flux<CustomerAddress> addresses = addressRepo.findByCustomerIdAndType(customerId, type);
		
				  
	  // @formatter:on

		return addresses;

	}

}
