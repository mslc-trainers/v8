package com.verizon.learning.web.util;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.verizon.learning.model.Customer;
import com.verizon.learning.model.CustomerAddress;
import com.verizon.learning.model.CustomerAddressRepository;
import com.verizon.learning.model.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class DataBuilder implements CommandLineRunner {

	@Autowired
	CustomerRepository customerRepo;
	
	@Autowired
	CustomerAddressRepository addressRepo;

    public List<CustomerAddress> addressData() {

        // @formatter:off


        return Arrays.asList(new CustomerAddress("9", "101", "101 line1", "101 line2", "101 city", "P"),
                new CustomerAddress("1", "101", "101 line1-a1", "101 line2-a1", "101 city-a1", "P"),
                new CustomerAddress("2", "105", "105 line1-a1", "105 line2", "105 city-a2", "D"),
                new CustomerAddress("3", "102", "102 line1", "102 line2", "102 city", "P"),
                new CustomerAddress("4", "103", "103 line1-1", "103 line2-1", "103 city-1", "P"),
                new CustomerAddress("5", "103", "103 line1-2", "103 line2-2", "103 city-2", "D"),
                new CustomerAddress("6", "103", "103 line1-3", "103 line2-3", "103 city-3", "P"),
                new CustomerAddress("7", "104", "104 line1", "104 line2", "104 city", "D"));

    }

    public List<Customer> customerData() {
        // @formatter:off

        return Arrays.asList(new Customer("101", "Verizon"),
                new Customer("102", "IBM"),
                new Customer("103", "Wipro"),
                new Customer("104", "JP Morgan"),
                new Customer("105", "Morgan Stanley"));


        // @formatter:on

	}

	@Override
	public void run(String... args) throws Exception {

		System.out.println("run of DataBuilder that implements CommandLineRunner is executed");

		initialize();

	}

	private void initialize() {

		/**
		 * a) delete all existing records from Customer table b) insert 5 new customers
		 * based on customerData() ArrayList c) findAll all the newly added data and
		 * print the same on console.
		 * 
		 */

		// @formatter:off
		customerRepo
		    .deleteAll()
		    .thenMany(Flux.fromIterable(customerData()))
		    .flatMap(x -> {
		    	
		    	Mono<Customer> newlyAddedCustomer = customerRepo.save(x);
		    	return newlyAddedCustomer;
		    }).thenMany(customerRepo.findAll())
		    .subscribe(x -> {
		    	System.out.println(x.getId() + " -- " + x.getName());
		    });
		    
		
		addressRepo
		    .deleteAll()
		    .thenMany(Flux.fromIterable(addressData()))
		    .flatMap(x -> {		    	
		    	return addressRepo.save(x);
		    })
		    .thenMany(addressRepo.findAll())
		    .subscribe(x -> {
		    		System.out.println(x);
		    	}
		    );
		
		
		 // @formatter:on

//		// a) deleting all Customers
//		Mono<Void> monoOfVoid =  customerRepo.deleteAll();
//		
//		List<Customer> customers = this.customerData();
//		
//		// b) inserting the customers from the list
//		for (Customer customer : customers) {
//			Mono<Customer> newlyAddedCustomer =  customerRepo.save(customer);
//		}
//		
////		c) findAll customers
//		
//		Flux<Customer> fCustomers = customerRepo.findAll();
//		
//		fCustomers
//		   .subscribe(x -> {
//			   System.out.println(x.getId() + " -- " + x.getName());
//		   });

	}

}
